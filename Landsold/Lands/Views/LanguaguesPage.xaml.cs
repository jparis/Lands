﻿

namespace Lands.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LanguaguesPage : ContentPage
	{
		public LanguaguesPage ()
		{
			InitializeComponent ();
		}
	}
}