﻿

namespace Lands.ViewModels
{
    using System.Collections.Generic;
    using Models;
    public class MainViewModel
    {
        #region Properties
        public List<Land> LandsList { get; set; }
        #endregion
        #region ViewModels


        public LandViewModel Land { get; set; }

        public LoginViewModel Login
        { get;
           set;

        }
        public LandsViewModel Lands { get; set; }
        #endregion
        #region Constructores
        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;
        #endregion

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        }
    }
}
