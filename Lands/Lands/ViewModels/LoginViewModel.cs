﻿



namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Lands.Views;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        

        #region Atributos
        private string email;
        private string password;
        private bool isActivo;
        private bool isEnabled;
        #endregion

        #region Propiedades
        public string Email
        {
            get { return this.email; }
            set { SetValue(ref this.email, value); }
        }
        public string Password
        {
            get { return this.password; }
            set { SetValue(ref this.password, value); }
        }
                
        public bool IsActivo {
            get { return this.isActivo; }
            set { SetValue(ref this.isActivo, value); }
        }
        public bool IsEnabled {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }
        #endregion

        #region Constructores
        public LoginViewModel()
        {
            this.IsActivo = true;
            this.IsEnabled = true;

                this.Email = "jparis@miralbueno.com";
            this.password = "1234";
        }
        #endregion

        #region Comandos
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }



        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Debes de introducir email",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "Debes de introducir password",
                    "Accept");
                return;
            }
            this.IsActivo = true;
            this.IsEnabled = false;
            if (this.Email != "jparis@miralbueno.com" || this.Password != "1234")
            {
                this.IsActivo = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "El email o el Password es incorrecto",
                    "Accept");
                this.Password = string.Empty;
                return;
            }
            this.IsActivo = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

            //aqui lo que hace es que no cree mas instancias de la MainViewMOdel si ya existe.
            MainViewModel.GetInstance().Lands = new LandsViewModel(); 
            await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());
        }
        #endregion

    }
}
